Task Tracker v2.0
Test Application

Requirements
PART 1:
   ⁃ Should load the data below into the existing data section on page load.
   ⁃ Should have the ability to create tasks that will immediately be prepended to the list.
   ⁃ Should not require a web server, and should be able to run offline.
   ⁃ Match the design as close as possible
   ⁃ All data exchanges should be in JSON format.
   ⁃ IMPORTANT: This must be completed using jQuery or vanilla Javascript - no other JS frameworks may be used.  Also, do not use any CSS frameworks (e.g. Bootstrap)
Existing Data (JSON)
[
   {"name": "Test Task #1", "date": "12/01/2012", "assigned": "John Doe" },
   {"name": "Test Task #2", "date": "12/02/2012", "assigned": "John Doe" },
   {"name": "Test Task #3", "date": "12/03/2012", "assigned": "John Doe" },
   {"name": "Test Task #4", "date": "12/04/2012", "assigned": "John Doe" },
   {"name": "Test Task #5", "date": "12/05/2012", "assigned": "John Doe" },
   {"name": "Test Task #6", "date": "12/06/2012", "assigned": "John Doe" },
   {"name": "Test Task #7", "date": "12/07/2012", "assigned": "John Doe" }
]

PART 2:
   ⁃ Convert the CSS developed in PART 1 into sass/scss which should be compiled using grunt/gulp
   - Convert js into modules
   - Include execution instructions

Deliver Part1 and Part2 separately.


PART 3: WRITING ASSIGNMENT
- Write about the most difficult problem you have faced during the development of an application. Also explain how you solved it.

- Explain which application (PART1 or PART2) is more scalable and why? Reason with examples.