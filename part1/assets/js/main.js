var setTasks = function () {
	let tasks = [ 
		{"name": "Test Task #1", "date": "12-01-2012", "assigned": "John Doe" },
		{"name": "Test Task #2", "date": "12-02-2012", "assigned": "John Doe" },
		{"name": "Test Task #3", "date": "12-03-2012", "assigned": "John Doe" }
	];

	const _offlineTasks = localStorage.getItem('tasks');
	
	if (_offlineTasks !== null) {
		tasks = JSON.parse(_offlineTasks); 
	} else {
		localStorage.setItem('tasks', JSON.stringify(tasks));
	}

	return tasks;
}

var updateView = function () {
	var tasks = setTasks();

	var html = '<ul>';

	for(var i = 0; i < tasks.length; i++) {
		html += '<li><strong>' + tasks[i].name + '</strong>';
		html += '<span>' + tasks[i].date + '</span>';
		html += '<em>' + tasks[i].assigned + '</em></li>';
	};

	html += '</ul>';

	document.getElementById('taskList').innerHTML = html;
}

var createTask = function () {
	var task = {
		name: document.getElementById('name').value,
		date: document.getElementById('date').value,
		assigned: document.getElementById('assigned').value
	}

	var tasks = setTasks();

	tasks.push(task);
	localStorage.setItem('tasks', JSON.stringify(tasks));

	updateView();
	resetForm();

	return false;
}

var resetForm = function () {
	document.getElementById('createTaskForm').reset();
}

document.getElementById('createTaskForm')
	.addEventListener('submit', function (event) {
		event.preventDefault();
		createTask();
	});

updateView();
