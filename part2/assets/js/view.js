var View = function () {	
	var initView = function () {
		$(window).on('load', function() {
			$('body').trigger(jQuery.Event('initView'));
		});

		$('#createTaskForm').on('submit', function(e) {
			var event = jQuery.Event('createTask');

			event.task = {
				name: $('#name')[0].value,
				date: $('#date')[0].value,
				assigned: $('#assigned')[0].value
			}

			$('body').trigger(event);
			$('#name')[0].value = '';
			$('#date')[0].value = '';
			$('#assigned')[0].value = '';

			e.preventDefault();
		});
	};

	var updateView = function (tasks) {	
		$('#taskList li').remove();
		
		for(var i = 0; i < tasks.length; i++) {
			$('#taskList')
				.append( 
					'<li>' + 
						'<strong>' + tasks[i].name + '</strong>' +
						'<span>' + tasks[i].date + '</span>' + 
						'<em>' + tasks[i].assigned + '</em>' + 
					'</li>'
				);
		}
	};

	initView();
		
	return {
		updateView: function (tasks) {
			updateView(tasks);
		}
	};
};
