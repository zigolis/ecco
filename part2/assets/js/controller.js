var Controller = function (view, model) {
   var _view = view;
   var _model = model;
   
   $('body').bind('createTask', function(e) { 
      _model.createTask( e.task );
   });

   $('body').bind('updateView', function() { 
      _view.updateView( _model.getData() );
   });

   $('body').bind('initView', function() { 
      _model.setData( _model.setData() );
      _view.updateView( _model.getData() );
   });
};
