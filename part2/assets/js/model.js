var Model = function () {
	var _tasks = [ 
		{'name': 'Test Task #1', 'date': '12-01-2012', 'assigned': 'John Doe' },
		{'name': 'Test Task #2', 'date': '12-02-2012', 'assigned': 'John Doe' },
		{'name': 'Test Task #3', 'date': '12-03-2012', 'assigned': 'John Doe' }
	];

	var setData = function () {
		_offlineTasks = localStorage.getItem('tasks');

		if (_offlineTasks === null) {
			localStorage.setItem('tasks', JSON.stringify(_tasks));
		} else {
			_tasks = JSON.parse(_offlineTasks); 
		}
	}

	var createTask = function (task) {
		_tasks.push(task);

		localStorage.setItem('tasks', JSON.stringify(_tasks));
	}

	var notifyController = function () {	
		$('body').trigger('updateView');
	}

	return  {
		getData: function () {
			return _tasks;
		},
		setData: function () {
			setData();
		},
		createTask: function ( task ) {
			createTask(task);
			notifyController();
		}
	};
};
