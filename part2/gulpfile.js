var gulp = require('gulp');
var sass = require('gulp-sass');
var useref = require('gulp-useref');
var uglify = require('gulp-uglify');
var gulpIf = require('gulp-if');
var del = require('del');
 
sass.compiler = require('node-sass');

gulp.task('sass', function () {
  return gulp.src('./assets/scss/index.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./assets/css'))
});

gulp.task('clean:dist', function() {
  return del.sync('dist');
})

gulp.task('useref', function(){
  return gulp.src('index.html')
    .pipe(useref())
    .pipe(gulpIf('*.js', uglify()))
    .pipe(gulp.dest('dist'))
});

function defaultTask(cb) {
  cb();
}

exports.default = defaultTask


